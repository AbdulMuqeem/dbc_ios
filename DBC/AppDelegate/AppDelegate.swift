//
//  AppDelegate.swift
//  DBC
//
//  Created by mac on 20/08/2020.
//  Copyright © 2020 Muqeem. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        navigateTOInitialViewController()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    static func getInstatnce() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func navigateTOInitialViewController() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let nav = RootViewController.instantiateFromStoryboard()
        self.window?.rootViewController = nav
        let vc = HomeViewController.instantiateFromStoryboard()
        nav.pushViewController(vc, animated: true)
        
    }
    
}

