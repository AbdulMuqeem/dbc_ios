//
//  WalletViewController.swift
//  DBC
//
//  Created by Abdul Muqeem on 22/08/2020.
//  Copyright © 2020 Muqeem. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> WalletViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! WalletViewController
    }
    
    @IBOutlet weak var tabBar:UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserManager.isUserLogin() {
            self.tabBar.items![4].image = UIImage(named: "profile_icon")
        }
        else {
            self.tabBar.items![4].image = UIImage(named: "settings_icon")
        }
        
        self.navigationController?.HideNavigationBar()
        self.tabBar.delegate = self
        self.tabBar.selectedItem = self.tabBar.items![1] as UITabBarItem
        
    }
    
}

extension WalletViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            self.tabBar.selectedItem = self.tabBar.items![1] as UITabBarItem
            return
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: CreateCardViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = CreateCardViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: BrowseViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = BrowseViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
            
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            if UserManager.isUserLogin() {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: ProfileViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                    else {
                        let vc = ProfileViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: false)
                        break
                    }
                }
                
            }
            else {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: SettingsViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                    else {
                        let vc = SettingsViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: false)
                        break
                    }
                }
            }
        }
    }
}
