//
//  HomeViewController.swift
//  DBC
//
//  Created by Abdul Muqeem on 22/08/2020.
//  Copyright © 2020 Muqeem. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    @IBOutlet weak var tabBar:UITabBar!
    @IBOutlet weak var browseView:UIView!
    @IBOutlet weak var tblView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:HomeCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: HomeCell.self))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserManager.isUserLogin() {
            self.tabBar.items![4].image = UIImage(named: "profile_icon")
        }
        else {
            self.tabBar.items![4].image = UIImage(named: "settings_icon")
            let vc = TutorialViewController.instantiateFromStoryboard()
            vc.modalPresentationStyle = UIModalPresentationStyle.custom
            self.present(vc, animated: true, completion: nil)
        }
        
        self.navigationController?.HideNavigationBar()
        self.tabBar.delegate = self
        self.tabBar.selectedItem = self.tabBar.items![0] as UITabBarItem
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.browseView.roundCorners(corners: [.topLeft, .topRight], radius: 60)
        
    }
    
}

extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(120)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:HomeCell = tableView.dequeueReusableCell(withIdentifier:String(describing:HomeCell.self)) as!  HomeCell
        
        return cell
    }
    
}

extension HomeViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            self.tabBar.selectedItem = self.tabBar.items![0] as UITabBarItem
            return
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            let vc = WalletViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            let vc = CreateCardViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            let vc = BrowseViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            if UserManager.isUserLogin() {
                let vc = ProfileViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else {
                let vc = SettingsViewController.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
}
