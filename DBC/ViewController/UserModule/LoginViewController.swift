//
//  LoginViewController.swift
//  DBC
//
//  Created by Abdul Muqeem on 20/08/2020.
//  Copyright © 2020 Muqeem. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    class func instantiateFromStoryboard() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}
