//
//  TutorialViewController.swift
//  DBC
//
//  Created by Abdul Muqeem on 26/08/2020.
//  Copyright © 2020 Muqeem. All rights reserved.
//

import UIKit

extension TutorialViewController : ViewPagerDataSource , SkipDelegate {
    
    func skipAction() {
        self.dismiss(animated: true, completion: nil)
    }
        
    func numberOfItems(viewPager:ViewPager) -> Int {
        return 3
    }
    
    func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {

        let view = TutorialView.instanceFromNib() as? TutorialView
        
        view?.lblText.text = "Welcome"
        view?.delegate = self
        
        return view!
        
    }
}

func didSelectedItem(index: Int) {
    print("select index \(index)")
}

class TutorialViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> TutorialViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TutorialViewController
    }
    
    
    @IBOutlet weak var viewPager:ViewPager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewPager.dataSource = self
        self.viewPager.pageControl.isUserInteractionEnabled = false

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

}

