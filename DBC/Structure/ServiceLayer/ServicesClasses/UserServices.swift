//
//  UserServices.swift
//  Food
//
//  Created by Abdul Muqeem on 24/10/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserServices {
    
    static func Login(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.login, parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["err"].array != nil {
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
    }
    
}
