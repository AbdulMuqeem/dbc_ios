//
//  Constants.swift
//  PaySii
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

public let THEME_COLOR: UInt = 0x3976B7

public let BASE_URL = "http://167.114.24.251:3099/" // Development
//public let BASE_URL = "https://paysii.com:99/" // Production

public let Google_Api_Key = "AIzaSyCBjDrjC7fCW7QaNxl18sFY5tfAuBnZW-0"

let APP_STORE_ID = "itms://itunes.apple.com/app/id1516461913"

public let LOGO_IMAGE: UIImage = UIImage(named:"logo")!
public let BACK_IMAGE: UIImage = UIImage(named:"back")!

// Alert
public let SUCCESS_IMAGE: UIImage = UIImage(named:"success")!
public let FAILURE_IMAGE: UIImage = UIImage(named:"error")!

public let WARNING:UIColor = UIColor.init(named: "Warning_Color")!
public let FAILURE:UIColor = UIColor.init(named: "Failure_Color")!
public let SUCCESS:UIColor = UIColor.init(named: "Success_Color")!

let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
let UIWINDOW                    = UIApplication.shared.delegate!.window!

// Extra

public let User_data_userDefault   = "User_data_userDefault"
public let token_userDefault   = "token_userDefault"


