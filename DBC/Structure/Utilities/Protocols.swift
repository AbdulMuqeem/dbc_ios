//
//  Protocol.swift
//  PaySii
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

protocol RefreshDelegate {
    func StartRefresh()
}

protocol DeliveryDelegate {
    func deliveryType(type:String , id:Int)
}

protocol SkipDelegate {
    func skipAction()
}

protocol AlertViewDelegate {
    func okAction()
}

protocol RegisterAlertViewDelegate {
    func loginAction()
}

protocol AlertViewDelegateAction {
    func okButtonAction()
    func cancelAction()
}

protocol CountryAlertViewDelegate {
    func emailAction()
    func cancelAction()
}

protocol GetDeliveryOptionDelegate {
    func getDelivery(indexpath:IndexPath)
}

protocol ViewReceiptActionDelegate {
    func getPDFReceipt(indexpath:IndexPath)
}

