//
//  CountryCell.swift
//  PaySii
//
//  Created by Abdul Muqeem on 19/12/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var imgCountry:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblAmount:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
