//
//  TopupCell.swift
//  PaySii
//
//  Created by Abdul Muqeem on 25/01/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class TopupCell: UICollectionViewCell {

    @IBOutlet weak var lblAmount:UILabel!
    @IBOutlet weak var imgSelect:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
