//
//  TutorialView.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 31/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class TutorialView: UIView {
    
    @IBOutlet weak var lblText:UILabel!
    @IBOutlet weak var pager:UIPageControl!
    
    var delegate:SkipDelegate?
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "TutorialView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    @IBAction func getSkipAction(_ sender:UIButton) {
        self.delegate!.skipAction()
    }
    
}
